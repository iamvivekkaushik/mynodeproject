const note = require('./notes.js');
const yargs = require('yargs');

var arg = yargs.argv;
var command = arg._[0];

switch(command) {
    case 'add':
        console.log(note.addNote(arg.title, arg.body));
        break;
    case 'remove':
        console.log(note.removeNote(arg.title));
        break;
    case 'list':
        note.getAll();
        break;
    case 'read':
        note.getNote(arg.title);
        break;
    default:
        console.log('- Notes : Command not recognized.')
}