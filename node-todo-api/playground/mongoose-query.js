const { ObjectID } = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {User} = require('./../server/models/user');

var id = "5bdedebf0bdc6409e90ee10c";

if(!ObjectID.isValid(id)) {
    console.log('Invalid Id');
    return;
}



User.findById(id).then((user) => {
    console.log(user);
}, (err) => {
    console.log(err);
});